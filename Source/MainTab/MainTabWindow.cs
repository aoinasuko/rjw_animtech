﻿using HarmonyLib;
using RimWorld;
using Rimworld_Animations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Linq;
using UnityEngine;
using Verse;

namespace BEP_AnimTech
{
    public class MainTabWindow_AnimTech : MainTabWindow
    {

        public override Vector2 RequestedTabSize => new Vector2(500, 800);

        // 編集中のページ
        private int windowpage = 0;

        // 編集中のActor
        private int currentactor = 0;

        // 編集中のステージ
        private int currentstage = 0;

        // 編集中のkeyframes
        private int currentkeyframes = 0;

        // 記憶しているアニメーションのDefname
        private static string MemoryDef;

        public void ResetAnim(Pawn curPawn, AnimationDef def)
        {
            for (int i = 0; i < curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation.Count; i++)
            {
                Pawn actor = curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation[i];

                RestartAnimation(actor.TryGetComp<CompBodyAnimator>(), i);

                //reset the clock time of every pawn in animation
                if (actor.jobs.curDriver is rjw.JobDriver_Sex)
                {
                    (actor.jobs.curDriver as rjw.JobDriver_Sex).ticks_left = def.animationTimeTicks;
                    (actor.jobs.curDriver as rjw.JobDriver_Sex).ticksLeftThisToil = def.animationTimeTicks;
                    (actor.jobs.curDriver as rjw.JobDriver_Sex).duration = def.animationTimeTicks;
                }
            }
        }

        // アニメーションをリセットしてテスト用に変更する
        public void RestartAnimation(CompBodyAnimator comp, int actor)
        {
            Traverse.Create(comp).Field("animTicks").SetValue(0);
            Traverse.Create(comp).Field("stageTicks").SetValue(0);
            Traverse.Create(comp).Field("clipTicks").SetValue(0);
            Traverse.Create(comp).Field("curStage").SetValue(0);
            Traverse.Create(comp).Field("controlGenitalAngle").SetValue(comp.CurrentAnimation.actors[actor].controlGenitalAngle);
            comp.tickAnim();
        }

        // アニメーション時間のリセット
        public void ReflashAnimtick(AnimationDef def)
        {
            def.animationTimeTicks = 0;
            foreach (AnimationStage stage in def.animationStages)
            {
                //stage.playTimeTicks = 0;
                foreach (BaseAnimationClip clip in stage.animationClips)
                {
                    PawnAnimationClip pawn_clip = (PawnAnimationClip)clip;
                    pawn_clip.GenitalAngle = new SimpleCurve();
                    pawn_clip.BodyAngle = new SimpleCurve();
                    pawn_clip.HeadAngle = new SimpleCurve();
                    pawn_clip.HeadBob = new SimpleCurve();
                    pawn_clip.BodyOffsetX = new SimpleCurve();
                    pawn_clip.BodyOffsetZ = new SimpleCurve();
                    pawn_clip.HeadFacing = new SimpleCurve();
                    pawn_clip.BodyFacing = new SimpleCurve();
                    pawn_clip.SoundEffects = new Dictionary<int, string>();
                    pawn_clip.quiver = new Dictionary<int, bool>();
                }
                stage.initialize();
                def.animationTimeTicks += stage.playTimeTicks;
            }
        }

        public override void DoWindowContents(Rect inRect)
        {

            Rect position = new Rect(inRect.x, inRect.y, inRect.width, inRect.height);


            Listing_Standard listingStandard = new Listing_Standard();
            listingStandard.Begin(position);

            listingStandard.Label("AnimTech Manager");

            listingStandard.GapLine();


            if (Find.Selector.SingleSelectedThing is Pawn)
            {

                Pawn curPawn = Find.Selector.SingleSelectedThing as Pawn;

                if (curPawn.TryGetComp<CompBodyAnimator>().isAnimating)
                {

                    AnimationDef def = curPawn.TryGetComp<CompBodyAnimator>().CurrentAnimation;

                    // リセット用コマンド
                    if (listingStandard.ButtonText("AnimTech.Tab.ResetAnim".Translate()))
                    {
                        ResetAnim(curPawn, def);
                    }

                    // アニメーションアウトプット
                    if (listingStandard.ButtonText("AnimTech.Tab.OutputAnim".Translate()))
                    {
                        String path = OutputXml(curPawn, def);
                        
                        Messages.Message("AnimTech.Tab.OutputAnimEnd".Translate(path), null, MessageTypeDefOf.PositiveEvent, false);
                    }

                    Rect rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 2, 20f);

                    // ページ戻し
                    if (Widgets.ButtonText(rect, "<="))
                    {
                        windowpage = Math.Max(windowpage - 1, 0);
                    }

                    rect = new Rect(listingStandard.ColumnWidth / 2, listingStandard.CurHeight, listingStandard.ColumnWidth / 2, 20f);

                    // ページ送り
                    if (Widgets.ButtonText(rect, "=>"))
                    {
                        windowpage = Math.Min(windowpage + 1, 2);
                    }

                    listingStandard.Gap(20f);

                    listingStandard.GapLine();

                    if (windowpage == 0)
                    {
                        /*
                        listingStandard.GapLine();
                        // アニメーションの記憶
                        if (listingStandard.ButtonText("Memory Anim"))
                        {
                            MemoryDef = def.defName;
                        }
                        // アニメーションの復元
                        if (listingStandard.ButtonText("Change to Memory Anim"))
                        {
                            def = DefDatabase<AnimationDef>.AllDefsListForReading.Where(x => x.defName == MemoryDef).FirstOrDefault();
                            ResetAnim(curPawn, def);
                        }
                        */
                        listingStandard.GapLine();
                        listingStandard.Label("AnimTech.Tab.CurrentPawnData".Translate() + curPawn.Name);
                        listingStandard.Label("AnimTech.Tab.ActorIndex".Translate() + curPawn.TryGetComp<CompBodyAnimator>().ActorIndex);
                        listingStandard.Label("AnimTech.Tab.BodyType".Translate() + curPawn.story.bodyType);
                        listingStandard.Label("AnimTech.Tab.Animation".Translate() + def.label);
                        listingStandard.Label("AnimTech.Tab.Mirror".Translate() + (curPawn.TryGetComp<CompBodyAnimator>().Mirror ? "AnimTech.Tab.Mirrored".Translate() : "AnimTech.Tab.MirrorNone".Translate()));
                        listingStandard.GapLine();

                        listingStandard.Label("Defname");

                        string defname = def.defName;

                        defname = listingStandard.TextEntry(defname);

                        if (defname == "")
                        {
                            def.defName = "null";
                        }
                        else
                        {
                            def.defName = defname;
                        }

                        listingStandard.CheckboxLabeled("AnimTech.Tab.Sound".Translate(), ref def.sounds);
                        listingStandard.Label("AnimTech.Tab.SexType".Translate());
                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);
                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Add".Translate()))
                        {
                            List<FloatMenuOption> list = new List<FloatMenuOption>();
                            foreach (rjw.xxx.rjwSextype Value in Enum.GetValues(typeof(rjw.xxx.rjwSextype)))
                            {
                                string Layername = Enum.GetName(typeof(rjw.xxx.rjwSextype), Value);

                                list.Add(new FloatMenuOption(Layername, delegate
                                {
                                    def.sexTypes.Add(Value);
                                }));
                            }
                            Find.WindowStack.Add(new FloatMenu(list));
                        }
                        rect = new Rect(listingStandard.ColumnWidth / 4 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 2, 20f);
                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Reset".Translate()))
                        {
                            def.sexTypes = new List<rjw.xxx.rjwSextype>();
                        }
                        listingStandard.Gap(25f);
                        foreach (rjw.xxx.rjwSextype sextype in def.sexTypes)
                        {
                            listingStandard.Label(sextype.ToString());
                        }
                    }

                    if (windowpage == 1)
                    {

                        // 動作の最大ステージ数
                        int maxstage = def.animationStages.Count;

                        listingStandard.Label("AnimTech.Tab.CurrentStage".Translate() + currentstage + "/" + (maxstage - 1));

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);

                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Remove".Translate()))
                        {
                            if (def.animationStages.Count > 1)
                            {
                                AnimationStage Stage = def.animationStages[currentstage];
                                def.animationTimeTicks -= Stage.playTimeTicks;
                                def.animationStages.RemoveAt(currentstage);
                                if (currentstage >= def.animationStages.Count)
                                {
                                    currentstage--;
                                }
                                for (int i = 0; i < curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation.Count; i++)
                                {
                                    Pawn actor = curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation[i];

                                    RestartAnimation(actor.TryGetComp<CompBodyAnimator>(), i);

                                    //reset the clock time of every pawn in animation
                                    if (actor.jobs.curDriver is rjw.JobDriver_Sex)
                                    {
                                        (actor.jobs.curDriver as rjw.JobDriver_Sex).ticks_left = def.animationTimeTicks;
                                        (actor.jobs.curDriver as rjw.JobDriver_Sex).ticksLeftThisToil = def.animationTimeTicks;
                                        (actor.jobs.curDriver as rjw.JobDriver_Sex).duration = def.animationTimeTicks;
                                    }
                                }
                            }
                        }
                        rect = new Rect(listingStandard.ColumnWidth / 4 + 5f, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);
                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Add".Translate()))
                        {
                            AnimationStage old_stages = def.animationStages[currentstage];
                            AnimationStage Stage = new AnimationStage();
                            Copy_Stage(ref Stage, old_stages);
                            def.animationTimeTicks += Stage.playTimeTicks;
                            def.animationStages.Insert(currentstage, Stage);
                            for (int i = 0; i < curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation.Count; i++)
                            {
                                Pawn actor = curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation[i];

                                RestartAnimation(actor.TryGetComp<CompBodyAnimator>(), i);

                                //reset the clock time of every pawn in animation
                                if (actor.jobs.curDriver is rjw.JobDriver_Sex)
                                {
                                    (actor.jobs.curDriver as rjw.JobDriver_Sex).ticks_left = def.animationTimeTicks;
                                    (actor.jobs.curDriver as rjw.JobDriver_Sex).ticksLeftThisToil = def.animationTimeTicks;
                                    (actor.jobs.curDriver as rjw.JobDriver_Sex).duration = def.animationTimeTicks;
                                }
                            }
                        }

                        listingStandard.Gap(25f);

                        currentstage = (int)listingStandard.Slider(currentstage, 0, def.animationStages.Count - 1);

                        listingStandard.Label("AnimTech.Tab.StageOption".Translate());
                        listingStandard.CheckboxLabeled("AnimTech.Tab.Looping".Translate(), ref def.animationStages[currentstage].isLooping);

                        listingStandard.Label("AnimTech.Tab.StageIndex".Translate() + def.animationStages[currentstage].stageIndex);

                        def.animationStages[currentstage].stageIndex = (int)listingStandard.Slider(def.animationStages[currentstage].stageIndex, 0, 10);

                        listingStandard.Label("AnimTech.Tab.TimeTicks".Translate() + def.animationStages[currentstage].playTimeTicks, 20f, "AnimTech.Tab.TimeTicks.Tips".Translate());

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-10"))
                        {
                            def.animationStages[currentstage].playTimeTicks -= 10;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-1"))
                        {
                            def.animationStages[currentstage].playTimeTicks -= 1;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+1"))
                        {
                            def.animationStages[currentstage].playTimeTicks += 1;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+10"))
                        {
                            def.animationStages[currentstage].playTimeTicks += 10;
                        }

                        listingStandard.Gap(25f);

                        PawnAnimationClip AnimationClip = (PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex];

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);

                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Layer".Translate()))
                        {
                            List<FloatMenuOption> list = new List<FloatMenuOption>();
                            foreach (AltitudeLayer Value in Enum.GetValues(typeof(AltitudeLayer)))
                            {
                                string Layername = Enum.GetName(typeof(AltitudeLayer), Value);

                                list.Add(new FloatMenuOption(Layername, delegate
                                {
                                    AnimationClip.layer = Value;
                                }));
                            }
                            Find.WindowStack.Add(new FloatMenu(list));
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 4 + 5f, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);

                        Widgets.Label(rect, ": " + AnimationClip.layer.ToString());

                    }

                    if (windowpage == 2)
                    {

                        // 動作の最大キーフレーム数
                        PawnAnimationClip AnimationClip = (PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex];

                        listingStandard.Label("AnimTech.Tab.CurrentKeyframe".Translate() + currentkeyframes + "/" + (AnimationClip.keyframes.Count - 1));

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);

                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Remove".Translate()))
                        {
                            if (AnimationClip.keyframes.Count > 1)
                            {
                                PawnKeyframe Keyframe = AnimationClip.keyframes[currentkeyframes];
                                AnimationClip.keyframes.RemoveAt(currentkeyframes);
                                if (currentkeyframes >= AnimationClip.keyframes.Count)
                                {
                                    currentkeyframes--;
                                }
                                for (int i = 0; i < curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation.Count; i++)
                                {
                                    Pawn actor = curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation[i];

                                    RestartAnimation(actor.TryGetComp<CompBodyAnimator>(), i);

                                    //reset the clock time of every pawn in animation
                                    if (actor.jobs.curDriver is rjw.JobDriver_Sex)
                                    {
                                        (actor.jobs.curDriver as rjw.JobDriver_Sex).ticks_left = def.animationTimeTicks;
                                        (actor.jobs.curDriver as rjw.JobDriver_Sex).ticksLeftThisToil = def.animationTimeTicks;
                                        (actor.jobs.curDriver as rjw.JobDriver_Sex).duration = def.animationTimeTicks;
                                    }
                                }
                            }
                        }
                        rect = new Rect(listingStandard.ColumnWidth / 4 + 5f, listingStandard.CurHeight, listingStandard.ColumnWidth / 4, 20f);
                        if (Widgets.ButtonText(rect, "AnimTech.Tab.Add".Translate()))
                        {
                            PawnKeyframe Current_Keyframe = AnimationClip.keyframes.ElementAt(currentkeyframes);
                            PawnKeyframe Keyframe = new PawnKeyframe();
                            Keyframe.bodyAngle    = Current_Keyframe.bodyAngle;
                            Keyframe.bodyFacing   = Current_Keyframe.bodyFacing;
                            Keyframe.bodyOffsetX  = Current_Keyframe.bodyOffsetX;
                            Keyframe.bodyOffsetZ  = Current_Keyframe.bodyOffsetZ;
                            Keyframe.genitalAngle = Current_Keyframe.genitalAngle;
                            Keyframe.headAngle    = Current_Keyframe.headAngle;
                            Keyframe.headBob      = Current_Keyframe.headBob;
                            Keyframe.headFacing   = Current_Keyframe.headFacing;
                            Keyframe.quiver       = Current_Keyframe.quiver;
                            Keyframe.soundEffect  = Current_Keyframe.soundEffect;
                            Keyframe.tickDuration = Current_Keyframe.tickDuration;
                            AnimationClip.keyframes.Insert(currentkeyframes, Keyframe);
                            for (int i = 0; i < curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation.Count; i++)
                            {
                                Pawn actor = curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation[i];

                                RestartAnimation(actor.TryGetComp<CompBodyAnimator>(), i);

                                //reset the clock time of every pawn in animation
                                if (actor.jobs.curDriver is rjw.JobDriver_Sex)
                                {
                                    (actor.jobs.curDriver as rjw.JobDriver_Sex).ticks_left = def.animationTimeTicks;
                                    (actor.jobs.curDriver as rjw.JobDriver_Sex).ticksLeftThisToil = def.animationTimeTicks;
                                    (actor.jobs.curDriver as rjw.JobDriver_Sex).duration = def.animationTimeTicks;
                                }
                            }
                        }

                        listingStandard.Gap(25f);

                        currentkeyframes = (int)listingStandard.Slider(currentkeyframes, 0, AnimationClip.keyframes.Count - 1);

                        rect = new Rect(0f, listingStandard.CurHeight, listingStandard.ColumnWidth / 2 - 20f, 20f);

                        if (Widgets.ButtonText(rect, "AnimTech.Tab.ResetParam".Translate()))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration = 10;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle = 0.00f;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle = 0.00f;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle = 0.00f;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ = 0.00f;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX = 0.00f;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob = 0.00f;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headFacing = 0;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyFacing = 0;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].soundEffect = null;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].quiver = false;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 2 - 10f, listingStandard.CurHeight, listingStandard.ColumnWidth / 2 - 20f, 20f);

                        if (Widgets.ButtonText(rect, "AnimTech.Tab.CopytoLast".Translate()))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().tickDuration = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().bodyAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().headAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().genitalAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().bodyOffsetZ = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().bodyOffsetX = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().headBob = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().headFacing = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headFacing;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().bodyFacing = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyFacing;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().soundEffect = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].soundEffect;
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes.Last().quiver = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].quiver;
                        }

                        listingStandard.Gap(25f);

                        listingStandard.Label("AnimTech.Tab.KeyframeOption".Translate());

                        listingStandard.Label("AnimTech.Tab.tickDuration".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration, 20f, "AnimTech.Tab.tickDuration.Tips".Translate());

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-10"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration -= 10;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration -= 1;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration += 1;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+10"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration += 10;
                        }

                        if (((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration < 1)
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].tickDuration = 1;
                        }

                        listingStandard.Gap(25f);

                        if (((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle == null)
                        {
                            if (currentkeyframes > 0)
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes - 1].bodyAngle;
                            }
                            else
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle = 0.0f;
                            }
                        }

                        listingStandard.Label("AnimTech.Tab.bodyAngle".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle);

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-1.0"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle -= 1.000f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle -= 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle -= 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle += 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 4 + 8f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle += 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 5 + 10f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+1.0"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyAngle += 1.000f;
                        }

                        listingStandard.Gap(22f);

                        if (((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle == null)
                        {
                            if (currentkeyframes > 0)
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes - 1].headAngle;
                            }
                            else
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle = 0.000f;
                            }
                        }

                        listingStandard.Label("AnimTech.Tab.headAngle".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle);

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-1.0"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle -= 1.000f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle -= 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle -= 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle += 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 4 + 8f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle += 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 5 + 10f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+1.0"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headAngle += 1.000f;
                        }

                        listingStandard.Gap(25f);

                        if (((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle == null)
                        {
                            if (currentkeyframes > 0)
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes - 1].genitalAngle;
                            }
                            else
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle = 0.000f;
                            }
                        }

                        listingStandard.Label("AnimTech.Tab.genitalAngle".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle);

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-1.0"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle -= 1.000f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle -= 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle -= 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle += 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 4 + 8f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle += 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 5 + 10f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+1.0"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle += 1.000f;
                        }

                        listingStandard.Gap(25f);

                        if (((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob == null)
                        {
                            if (currentkeyframes > 0)
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].genitalAngle = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes - 1].headBob;
                            }
                            else
                            {
                                ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob = 0.000f;
                            }
                        }

                        listingStandard.Label("AnimTech.Tab.headBob".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob);

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob -= 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob -= 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob += 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headBob += 0.100f;
                        }

                        listingStandard.Gap(25f);

                        listingStandard.Label("AnimTech.Tab.bodyOffsetZ".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ);

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ -= 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ -= 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ += 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetZ += 0.100f;
                        }

                        listingStandard.Gap(25f);

                        listingStandard.Label("AnimTech.Tab.bodyOffsetX".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX);

                        rect = new Rect(0, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX -= 0.100f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 + 2f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "-0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX -= 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 2 + 4f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.01"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX += 0.010f;
                        }

                        rect = new Rect(listingStandard.ColumnWidth / 8 * 3 + 6f, listingStandard.CurHeight, listingStandard.ColumnWidth / 8, 20f);

                        if (Widgets.ButtonText(rect, "+0.1"))
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyOffsetX += 0.100f;
                        }

                        listingStandard.Gap(25f);

                        rect = new Rect(listingStandard.ColumnWidth / 2, listingStandard.CurHeight, listingStandard.ColumnWidth / 2, 25f);

                        string soundname = ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].soundEffect ?? "";

                        listingStandard.Label("AnimTech.Tab.soundEffect".Translate(), 20f, "AnimTech.Tab.soundEffect.Tips".Translate());

                        soundname = listingStandard.TextEntry(soundname);

                        if (soundname == "")
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].soundEffect = null;
                        }
                        else
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].soundEffect = soundname;
                        }

                        if (((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].quiver == null)
                        {
                            ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].quiver = false;
                        }

                        bool flagquiver = (bool)((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].quiver;

                        listingStandard.CheckboxLabeled("AnimTech.Tab.Quiver".Translate(), ref flagquiver);

                        ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].quiver = flagquiver;

                        listingStandard.Label("AnimTech.Tab.BodyFaceing".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyFacing);

                        ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyFacing = (int)listingStandard.Slider((float)((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].bodyFacing, 0, 3);

                        listingStandard.Label("AnimTech.Tab.HeadFacing".Translate() + ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headFacing);

                        ((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headFacing = (int)listingStandard.Slider((float)((PawnAnimationClip)def.animationStages[currentstage].animationClips[curPawn.TryGetComp<CompBodyAnimator>().ActorIndex]).keyframes[currentkeyframes].headFacing, 0, 3);

                    }

                    ReflashAnimtick(def);

                    /*
                    // もし変更で即終了になっている場合は元に戻す
                    if ((int)Traverse.Create(curPawn.TryGetComp<CompBodyAnimator>()).Field("animTicks").GetValue() >= def.animationTimeTicks)
                    {
                        for (int i = 0; i < curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation.Count; i++)
                        {
                            Pawn actor = curPawn.TryGetComp<CompBodyAnimator>().actorsInCurrentAnimation[i];

                            RestartAnimation(actor.TryGetComp<CompBodyAnimator>(), i);

                            //reset the clock time of every pawn in animation
                            if (actor.jobs.curDriver is rjw.JobDriver_Sex)
                            {
                                (actor.jobs.curDriver as rjw.JobDriver_Sex).ticks_left = def.animationTimeTicks;
                                (actor.jobs.curDriver as rjw.JobDriver_Sex).ticksLeftThisToil = def.animationTimeTicks;
                                (actor.jobs.curDriver as rjw.JobDriver_Sex).duration = def.animationTimeTicks;
                            }
                        }
                    }
                    */

                }

            }
            else
            {
                listingStandard.Label("AnimTech.Tab.NonSelect".Translate());
            }

            listingStandard.End();

        }

        // Defをアップロードする
        public string OutputXml(Pawn curPawn, AnimationDef def)
        {
            string output = GenFilePaths.ModsFolderPath + "\\AnimTech_output";
            DirectoryInfo directoryInfo = new DirectoryInfo(output);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.IndentChars = "\t";
            string outputPath = Path.Combine(output, def.defName + ".xml");
            using (XmlWriter xmlWriter = XmlWriter.Create(outputPath, xmlWriterSettings))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("Defs");
                xmlWriter.WriteStartElement("Rimworld_Animations.AnimationDef");
                xmlWriter.WriteElementString("defName", def.defName);
                xmlWriter.WriteElementString("label", def.label);
                xmlWriter.WriteElementString("sounds", def.sounds.ToString());
                xmlWriter.WriteStartElement("sexTypes");
                foreach (rjw.xxx.rjwSextype sextype in def.sexTypes)
                {
                    xmlWriter.WriteElementString("li", sextype.ToString());
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("actors");
                foreach (Actor actor in def.actors)
                {
                    xmlWriter.WriteStartElement("li");
                        if (!actor.defNames.NullOrEmpty())
                        {
                            xmlWriter.WriteStartElement("defNames");
                            foreach (string defNames in actor.defNames)
                            {
                                xmlWriter.WriteElementString("li", defNames);
                            }
                            xmlWriter.WriteEndElement();
                        }
                        if (!actor.requiredGenitals.NullOrEmpty())
                        {
                            xmlWriter.WriteStartElement("requiredGenitals");
                            foreach (string requiredGenitals in actor.requiredGenitals)
                            {
                                xmlWriter.WriteElementString("li", requiredGenitals);
                            }
                            xmlWriter.WriteEndElement();
                        }
                        xmlWriter.WriteElementString("initiator", actor.initiator.ToString());
                        if (!actor.gender.NullOrEmpty())
                        {
                            xmlWriter.WriteElementString("gender", actor.gender);
                        }
                        xmlWriter.WriteElementString("isFucking", actor.isFucking.ToString());
                        xmlWriter.WriteElementString("isFucked", actor.isFucked.ToString());
                        xmlWriter.WriteElementString("controlGenitalAngle", actor.controlGenitalAngle.ToString());
                        if (!actor.bodyDefTypes.NullOrEmpty())
                        {
                            xmlWriter.WriteStartElement("bodyDefTypes");
                            foreach (BodyDef bodyDefTypes in actor.bodyDefTypes)
                            {
                                xmlWriter.WriteElementString("li", bodyDefTypes.ToString());
                            }
                            xmlWriter.WriteEndElement();
                        }
                        xmlWriter.WriteStartElement("bodyTypeOffset");
                            if (actor.bodyTypeOffset.Male.HasValue)
                            {
                                xmlWriter.WriteElementString("Male", actor.bodyTypeOffset.Male.ToString());
                            }
                            if (actor.bodyTypeOffset.Female.HasValue) 
                            {
                                xmlWriter.WriteElementString("Female", actor.bodyTypeOffset.Female.ToString());
                            }
                            if (actor.bodyTypeOffset.Thin.HasValue)
                            {
                                xmlWriter.WriteElementString("Thin", actor.bodyTypeOffset.Thin.ToString());
                            }
                            if (actor.bodyTypeOffset.Hulk.HasValue)
                            {
                                xmlWriter.WriteElementString("Hulk", actor.bodyTypeOffset.Hulk.ToString());
                            }
                            if (actor.bodyTypeOffset.Fat.HasValue)
                            {
                                xmlWriter.WriteElementString("Fat", actor.bodyTypeOffset.Fat.ToString());
                            }
                        xmlWriter.WriteEndElement();
                        if (!actor.requiredGender.NullOrEmpty())
                        {
                            xmlWriter.WriteStartElement("requiredGender");
                            foreach (string requiredGender in actor.requiredGender)
                            {
                                xmlWriter.WriteElementString("li", requiredGender);
                            }
                            xmlWriter.WriteEndElement();
                        }
                    xmlWriter.WriteEndElement();
                }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("animationStages");
                    foreach (AnimationStage animationStages in def.animationStages)
                    {
                        xmlWriter.WriteStartElement("li");
                            xmlWriter.WriteElementString("stageName", animationStages.stageName);
                            xmlWriter.WriteElementString("stageIndex", animationStages.stageIndex.ToString());
                            xmlWriter.WriteElementString("playTimeTicks", animationStages.playTimeTicks.ToString());
                            xmlWriter.WriteElementString("isLooping", animationStages.isLooping.ToString());
                            if (!animationStages.animationClips.NullOrEmpty())
                            {
                                xmlWriter.WriteStartElement("animationClips");
                                foreach (BaseAnimationClip animationClips in animationStages.animationClips)
                                {
                                    PawnAnimationClip Pawn_animationClips = (PawnAnimationClip)animationClips;
                                    xmlWriter.WriteStartElement("li");
                                    xmlWriter.WriteAttributeString("Class", "Rimworld_Animations.PawnAnimationClip");
                                        xmlWriter.WriteElementString("layer", Pawn_animationClips.layer.ToString());
                                        xmlWriter.WriteStartElement("keyframes");
                                        foreach (PawnKeyframe keyframes in Pawn_animationClips.keyframes)
                                        {
                                            xmlWriter.WriteStartElement("li");
                                                xmlWriter.WriteElementString("tickDuration", keyframes.tickDuration.ToString());
                                                if (keyframes.bodyAngle.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("bodyAngle", keyframes.bodyAngle.ToString());
                                                }
                                                if (keyframes.headAngle.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("headAngle", keyframes.headAngle.ToString());
                                                }
                                                if (keyframes.genitalAngle.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("genitalAngle", keyframes.genitalAngle.ToString());
                                                }
                                                if (keyframes.bodyOffsetX.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("bodyOffsetX", keyframes.bodyOffsetX.ToString());
                                                }
                                                if (keyframes.bodyOffsetZ.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("bodyOffsetZ", keyframes.bodyOffsetZ.ToString());
                                                }
                                                if (keyframes.headBob.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("headBob", keyframes.headBob.ToString());
                                                }
                                                if (keyframes.bodyFacing.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("bodyFacing", keyframes.bodyFacing.ToString());
                                                }
                                                if (keyframes.headFacing.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("headFacing", keyframes.headFacing.ToString());
                                                }
                                                if (!keyframes.soundEffect.NullOrEmpty())
                                                {
                                                    xmlWriter.WriteElementString("soundEffect", keyframes.soundEffect);
                                                }
                                                if (keyframes.quiver.HasValue)
                                                {
                                                    xmlWriter.WriteElementString("quiver", keyframes.quiver.ToString());
                                                }
                                            xmlWriter.WriteEndElement();
                                        }
                                        xmlWriter.WriteEndElement();
                                    xmlWriter.WriteEndElement();
                                }
                                xmlWriter.WriteEndElement();
                            }
                        xmlWriter.WriteEndElement();
                    }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
            }
            return outputPath;
        }

        // Stageのコピーを行う
        public void Copy_Stage(ref AnimationStage new_stage, AnimationStage old_stage)
        {
            new_stage.animationClips = new List<BaseAnimationClip>();
            foreach (BaseAnimationClip clip in old_stage.animationClips)
            {
                PawnAnimationClip new_clip = new PawnAnimationClip();
                new_clip.keyframes = new List<PawnKeyframe>();
                foreach (PawnKeyframe keyframe in ((PawnAnimationClip)clip).keyframes)
                {
                    PawnKeyframe new_frame = new PawnKeyframe();
                    Copy_Frame(ref new_frame, keyframe);
                    new_clip.keyframes.Add(new_frame);
                }
                new_clip.layer = ((PawnAnimationClip)clip).layer;
                new_clip.buildSimpleCurves();
                new_stage.animationClips.Add(new_clip);
            }
            new_stage.stageName     = old_stage.stageName;
            new_stage.stageIndex    = old_stage.stageIndex;
            new_stage.isLooping     = old_stage.isLooping;
            new_stage.playTimeTicks = old_stage.playTimeTicks;

        }

        // Frameのコピーを行う
        public void Copy_Frame(ref PawnKeyframe new_frame, PawnKeyframe old_frame)
        {
            new_frame.bodyAngle     = old_frame.bodyAngle;
            new_frame.bodyFacing    = old_frame.bodyFacing;
            new_frame.bodyOffsetX   = old_frame.bodyOffsetX;
            new_frame.bodyOffsetZ   = old_frame.bodyOffsetZ;
            new_frame.genitalAngle  = old_frame.genitalAngle;
            new_frame.headAngle     = old_frame.headAngle;
            new_frame.headBob       = old_frame.headBob;
            new_frame.headFacing    = old_frame.headFacing;
            new_frame.quiver        = old_frame.quiver;
            new_frame.soundEffect   = old_frame.soundEffect;
            new_frame.tickDuration  = old_frame.tickDuration;
        }


        public override void PreOpen()
        {
            base.PreOpen();
            currentstage = 0;
            currentactor = 0;
            currentkeyframes = 0;
            windowpage = 0;
        }

        public override void PostClose()
        {
            base.PostClose();
        }

    }

}
