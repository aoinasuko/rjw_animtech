﻿using HarmonyLib;
using RimWorld;
using rjw;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Verse;

namespace BEP_AnimTech
{

    [StaticConstructorOnStartup]
    static class AnimTech_Harmony
    {
        static AnimTech_Harmony()
        {
            var harmony = new Harmony("BEP.AnimTech");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
    }

}
